variable "name" {}
variable "region" {}
variable "access_key" {}
variable "secret_key" {}

variable "key_name" {}
variable "vpc_cidr_block" {}
variable "dmz_cidr_block" {}

variable "main_instance_private_ip"{}
variable "ami" {}
variable  "instance_type" {}



variable "availability_zone_dmz" {}
