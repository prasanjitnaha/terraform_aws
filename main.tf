provider "aws" {
  profile    = "default"
  region     = var.region
  access_key = var.access_key
  secret_key = var.secret_key

  version = "~> 2.0"
}
# EC2 instance

data "template_file" "start" {
  template = "${file("script.tpl")}"
}
resource "aws_instance" "main" {
  ami                    = var.ami
  instance_type          = var.instance_type
  subnet_id              = aws_subnet.dmz.id
  key_name               = var.key_name
  user_data              = "${data.template_file.start.rendered}"
  vpc_security_group_ids = [aws_security_group.base.id]
  private_ip             = var.main_instance_private_ip
}

resource "aws_security_group" "base" {
  name        = "Base SG"

  # Outbound HTTPS
  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Outbound HTTP
  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 21
    to_port     = 21
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }


  # Allow inbound SSH
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    self        = false
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    self        = false
  }

   ingress {
    from_port   = 21
    to_port     = 21
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    self        = false
  }

  ingress {
   from_port   = 80
   to_port     = 80
   protocol    = "tcp"
   cidr_blocks = ["0.0.0.0/0"]
   self        = false
 }

  vpc_id = aws_vpc.main.id

}

# Elastic IP
resource "aws_eip" "main_eip" {
  vpc = true
}

resource "aws_eip_association" "main_eip_assoc" {
  instance_id   = aws_instance.main.id
  allocation_id = aws_eip.main_eip.id
}

# VPC
resource "aws_vpc" "main" {
  cidr_block       = var.vpc_cidr_block
}

# Internet gateway
resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id
}

# Public Route Table
resource "aws_route_table" "main_public" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main.id
  }
}

# DMZ
resource "aws_subnet" "dmz" {
  vpc_id            = aws_vpc.main.id
  cidr_block       =  var.dmz_cidr_block
  availability_zone = var.availability_zone_dmz



}

# Route Table Assoc for DMZ
resource "aws_route_table_association" "dmz" {
  subnet_id      = aws_subnet.dmz.id
  route_table_id = aws_route_table.main_public.id
}
